import pandas as pd
import numpy as np
from pandas import read_csv
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression as LR


def auto_correlation_data_prep(p, url = 'https://raw.githubusercontent.com/jbrownlee/Datasets/master/daily-total-female-births.csv'):
    df = pd.read_csv(url)
    for i in range(1, p+1):
        new_name = f'shifted_{i}'
        df[new_name] = df.Births.shift(i)
    return df

total_lag=21
temp_df = auto_correlation_data_prep(total_lag)
female_birth_df = temp_df.iloc[total_lag:]


train_index = int(len(female_birth_df)*0.67)

D_train = female_birth_df.iloc[:train_index]
D_test = female_birth_df.iloc[train_index:]


results = []
for lag in range(3, total_lag):
    X_train = D_train.values[:,2:lag]
    y_train = D_train["Births"]
    X_test = D_test.values[:,2:lag]
    y_test = D_test["Births"]
    reg = LR(fit_intercept=True)
    reg.fit(X_train, y_train)
    results.append(
        (lag,
         ((reg.predict(X_train) - y_train) ** 2).mean(),
         ((reg.predict(X_test) - y_test) ** 2).mean()))
results = np.asarray(results)
plt.plot(results[:, 0].astype(int), results[:, 1], label="train")
plt.plot(results[:, 0].astype(int), results[:, 2], label="test")
plt.legend()
plt.show()

